<?php
defined('TYPO3') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'DrkserviceAppButtons',
            'Appbuttonlist',
            [
                \DrkService\DrkserviceAppButtons\Controller\AppButtonListController::class => 'list, openSso'
            ],
            // non-cacheable actions
            [
                \DrkService\DrkserviceAppButtons\Controller\AppButtonListController::class => 'openSso'
            ]
        );

        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems.plugins {
                    elements {
                        appbuttonlist {
                            iconIdentifier = drkservice_app_buttons-plugin-appbuttonlist
                            title = LLL:EXT:drkservice_app_buttons/Resources/Private/Language/locallang_db.xlf:tx_drkservice_app_buttons_appbuttonlist.name
                            description = LLL:EXT:drkservice_app_buttons/Resources/Private/Language/locallang_db.xlf:tx_drkservice_app_buttons_appbuttonlist.description
                            tt_content_defValues {
                                CType = list
                                list_type = drkserviceappbuttons_appbuttonlist
                            }
                        }
                    }
                    show = *
                }
           }'
        );
		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);

        $iconRegistry->registerIcon(
            'drkservice_app_buttons-plugin-appbuttonlist',
            \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
            ['source' => 'EXT:drkservice_app_buttons/Resources/Public/Icons/Extension.svg']
        );

    }
);
