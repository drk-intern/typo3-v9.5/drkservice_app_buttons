<?php

/***************************************************************
 * Extension Manager/Repository config file for ext: "drkservice_app_buttons"
 *
 * Auto generated by Extension Builder 2020-09-09
 *
 * Manual updates:
 * Only the data in the array - anything else is removed by next write.
 * "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF['drkservice_app_buttons'] = [
    'title' => 'DRK Service App Buttons',
    'description' => 'App Buttons for DRK-Intern',
    'category' => 'plugin',
    'author' => 'André Gyöngyösi',
    'author_email' => 'a.gyoengyoesi@drkservice.de',
    'state' => 'alpha',
    'version' => '12.10.0',
    'constraints' => [
        'depends' => [
            'typo3' => '10.4.0-10.4.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
