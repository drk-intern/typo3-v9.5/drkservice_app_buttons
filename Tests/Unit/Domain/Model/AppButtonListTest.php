<?php
namespace DrkService\DrkserviceAppButtons\Tests\Unit\Domain\Model;

use TYPO3\TestingFramework\Core\Unit\UnitTestCase;
use DrkService\DrkserviceAppButtons\Domain\Model\AppButtonList;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use DrkService\DrkserviceAppButtons\Domain\Model\AppButton;
/**
 * Test case.
 *
 * @author André Gyöngyösi <a.gyoengyoesi@drkservice.de>
 */
class AppButtonListTest extends UnitTestCase
{
    /**
     * @var AppButtonList
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new AppButtonList();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getAppButtonReturnsInitialValueForAppButton()
    {
        $newObjectStorage = new ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getAppButton()
        );
    }

    /**
     * @test
     */
    public function setAppButtonForObjectStorageContainingAppButtonSetsAppButton()
    {
        $appButton = new AppButton();
        $objectStorageHoldingExactlyOneAppButton = new ObjectStorage();
        $objectStorageHoldingExactlyOneAppButton->attach($appButton);
        $this->subject->setAppButton($objectStorageHoldingExactlyOneAppButton);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneAppButton,
            'appButton',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addAppButtonToObjectStorageHoldingAppButton()
    {
        $appButton = new AppButton();
        $appButtonObjectStorageMock = $this->getMockBuilder(ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $appButtonObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($appButton));
        $this->inject($this->subject, 'appButton', $appButtonObjectStorageMock);

        $this->subject->addAppButton($appButton);
    }

    /**
     * @test
     */
    public function removeAppButtonFromObjectStorageHoldingAppButton()
    {
        $appButton = new AppButton();
        $appButtonObjectStorageMock = $this->getMockBuilder(ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $appButtonObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($appButton));
        $this->inject($this->subject, 'appButton', $appButtonObjectStorageMock);

        $this->subject->removeAppButton($appButton);
    }
}
