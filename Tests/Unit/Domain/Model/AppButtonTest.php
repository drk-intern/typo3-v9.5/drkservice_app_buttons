<?php
namespace DrkService\DrkserviceAppButtons\Tests\Unit\Domain\Model;

use TYPO3\TestingFramework\Core\Unit\UnitTestCase;
use DrkService\DrkserviceAppButtons\Domain\Model\AppButton;
use TYPO3\CMS\Extbase\Domain\Model\FileReference;
/**
 * Test case.
 *
 * @author André Gyöngyösi <a.gyoengyoesi@drkservice.de>
 */
class AppButtonTest extends UnitTestCase
{
    /**
     * @var AppButton
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new AppButton();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getButtonNameReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getButtonName()
        );
    }

    /**
     * @test
     */
    public function setButtonNameForStringSetsButtonName()
    {
        $this->subject->setButtonName('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'buttonName',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getButtonDescriptionReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getButtonDescription()
        );
    }

    /**
     * @test
     */
    public function setButtonDescriptionForStringSetsButtonDescription()
    {
        $this->subject->setButtonDescription('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'buttonDescription',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getButtonImageReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getButtonImage()
        );
    }

    /**
     * @test
     */
    public function setButtonImageForFileReferenceSetsButtonImage()
    {
        $fileReferenceFixture = new FileReference();
        $this->subject->setButtonImage($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'buttonImage',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getButtonLinkLoggedInReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getButtonLinkLoggedIn()
        );
    }

    /**
     * @test
     */
    public function setButtonLinkLoggedInForStringSetsButtonLinkLoggedIn()
    {
        $this->subject->setButtonLinkLoggedIn('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'buttonLinkLoggedIn',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getButtonLinkLoggedOutReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getButtonLinkLoggedOut()
        );
    }

    /**
     * @test
     */
    public function setButtonLinkLoggedOutForStringSetsButtonLinkLoggedOut()
    {
        $this->subject->setButtonLinkLoggedOut('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'buttonLinkLoggedOut',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getButtonSsoReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getButtonSso()
        );
    }

    /**
     * @test
     */
    public function setButtonSsoForBoolSetsButtonSso()
    {
        $this->subject->setButtonSso(true);

        self::assertAttributeEquals(
            true,
            'buttonSso',
            $this->subject
        );
    }
}
