<?php
namespace DrkService\DrkserviceAppButtons\Tests\Unit\Controller;

use TYPO3\TestingFramework\Core\Unit\UnitTestCase;
use DrkService\DrkserviceAppButtons\Controller\AppButtonListController;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use TYPO3\CMS\Extbase\Mvc\View\ViewInterface;
use DrkService\DrkserviceAppButtons\Domain\Repository\AppButtonListRepository;

/**
 * Test case.
 *
 * @author André Gyöngyösi <a.gyoengyoesi@drkservice.de>
 */
class AppButtonListControllerTest extends UnitTestCase
{
    /**
     * @var AppButtonListController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(AppButtonListController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllAppButtonListsFromRepositoryAndAssignsThemToView()
    {

        $allAppButtonLists = $this->getMockBuilder(ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $appButtonListRepository = $this->getMockBuilder(AppButtonListRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $appButtonListRepository->expects(self::once())->method('findAll')->will(self::returnValue($allAppButtonLists));
        $this->inject($this->subject, 'appButtonListRepository', $appButtonListRepository);

        $view = $this->getMockBuilder(ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('appButtonLists', $allAppButtonLists);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }
}
