#
# Table structure for table 'tx_drkserviceappbuttons_domain_model_appbutton'
#
CREATE TABLE tx_drkserviceappbuttons_domain_model_appbutton (

	appbuttonlist int(11) unsigned DEFAULT '0' NOT NULL,

	button_name varchar(255) DEFAULT '' NOT NULL,
	button_description varchar(255) DEFAULT '' NOT NULL,
	button_image int(11) unsigned NOT NULL default '0',
	button_link_logged_in varchar(255) DEFAULT '' NOT NULL,
	button_link_logged_out varchar(255) DEFAULT '' NOT NULL,
	button_sso smallint(5) unsigned DEFAULT '0' NOT NULL

);

#
# Table structure for table 'tx_drkserviceappbuttons_domain_model_appbuttonlist'
#
CREATE TABLE tx_drkserviceappbuttons_domain_model_appbuttonlist (

	app_button int(11) unsigned DEFAULT '0' NOT NULL

);
