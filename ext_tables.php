<?php
defined('TYPO3') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'DrkserviceAppButtons',
            'Appbuttonlist',
            'DRK App Button List'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('drkservice_app_buttons', 'Configuration/TypoScript', 'DRK Service App Buttons');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_drkserviceappbuttons_domain_model_appbutton', 'EXT:drkservice_app_buttons/Resources/Private/Language/locallang_csh_tx_drkserviceappbuttons_domain_model_appbutton.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_drkserviceappbuttons_domain_model_appbuttonlist', 'EXT:drkservice_app_buttons/Resources/Private/Language/locallang_csh_tx_drkserviceappbuttons_domain_model_appbuttonlist.xlf');

    }
);
