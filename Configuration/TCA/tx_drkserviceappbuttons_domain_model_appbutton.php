<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:drkservice_app_buttons/Resources/Private/Language/locallang_db.xlf:tx_drkserviceappbuttons_domain_model_appbutton',
        'label' => 'button_name',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'sortby' => 'sorting',
        'versioningWS' => true,
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'security' => [
            'ignorePageTypeRestriction' => true
        ],
        'searchFields' => 'button_name,button_description,button_link_logged_in,button_link_logged_out',
        'iconfile' => 'EXT:drkservice_app_buttons/Resources/Public/Icons/tx_drkserviceappbuttons_domain_model_appbutton.png'
    ],
    'types' => [
        '1' => ['showitem' => 'hidden,button_name,button_description,button_image,button_link_logged_in,button_link_logged_out,button_sso,--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access,starttime,endtime'],
    ],
    'columns' => [
        't3ver_label' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.visible',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        'label' => '',
                        'invertStateDisplay' => true
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'datetime',
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'datetime',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],

        'button_name' => [
            'exclude' => true,
            'label' => 'LLL:EXT:drkservice_app_buttons/Resources/Private/Language/locallang_db.xlf:tx_drkserviceappbuttons_domain_model_appbutton.button_name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'button_description' => [
            'exclude' => true,
            'label' => 'LLL:EXT:drkservice_app_buttons/Resources/Private/Language/locallang_db.xlf:tx_drkserviceappbuttons_domain_model_appbutton.button_description',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'button_image' => [
            'exclude' => true,
            'label' => 'LLL:EXT:drkservice_app_buttons/Resources/Private/Language/locallang_db.xlf:tx_drkserviceappbuttons_domain_model_appbutton.button_image',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'button_image',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference'
                    ],
                    'foreign_match_fields' => [
                        'fieldname' => 'button_image',
                        'tablenames' => 'tx_drkserviceappbuttons_domain_model_appbutton',
                    ],
                    'maxitems' => 1,
                    'overrideChildTca' => ['types' => [
                        '0' => [
                            'showitem' => '
                            --palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                            'showitem' => '
                            --palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                            'showitem' => '
                            --palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                            'showitem' => '
                            --palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                            'showitem' => '
                            --palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                            'showitem' => '
                            --palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ]
                    ]]
                ],
                $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
            ),

        ],
        'button_link_logged_in' => [
            'exclude' => true,
            'label' => 'LLL:EXT:drkservice_app_buttons/Resources/Private/Language/locallang_db.xlf:tx_drkserviceappbuttons_domain_model_appbutton.button_link_logged_in',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'button_link_logged_out' => [
            'exclude' => true,
            'label' => 'LLL:EXT:drkservice_app_buttons/Resources/Private/Language/locallang_db.xlf:tx_drkserviceappbuttons_domain_model_appbutton.button_link_logged_out',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'button_sso' => [
            'exclude' => true,
            'label' => 'LLL:EXT:drkservice_app_buttons/Resources/Private/Language/locallang_db.xlf:tx_drkserviceappbuttons_domain_model_appbutton.button_sso',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_core.xlf:labels.enabled'
                    ]
                ],
                'default' => 0,
            ]
        ],

        'appbuttonlist' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
    ],
];
