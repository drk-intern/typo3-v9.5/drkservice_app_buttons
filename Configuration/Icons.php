<?php

return [
    'drk-logo-icon' => [
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        'source' => 'EXT:drkservice_app_buttons/Resources/Public/Icons/drk-logo-icon.svg',
    ],
];
