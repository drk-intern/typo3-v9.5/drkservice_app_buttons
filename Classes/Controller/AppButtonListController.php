<?php
namespace DrkService\DrkserviceAppButtons\Controller;

use Psr\Http\Message\MessageInterface;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Core\Context\Exception\AspectNotFoundException;
use UnexpectedValueException;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Context\Context;
use DrkService\DrkserviceAppButtons\Domain\Model\AppButton;
use DrkService\DrkserviceAppButtons\Domain\Repository\AppButtonListRepository;
use DrkService\DrkserviceAppButtons\Domain\Repository\AppButtonRepository;

/***
 *
 * This file is part of the "DRK Service App Buttons" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2025 André Gyöngyösi <a.gyoengyoesi@drkservice.de>, DRK Service GmbH
 *
 ***/
/**
 * AppButtonListController
 */
class AppButtonListController extends ActionController
{
    /**
     * @var AppButtonListRepository
     */
    protected AppButtonListRepository $appButtonListRepository;

    /**
     * @var
     */
    protected $context;

    /**
     * @var string
     */
    protected string $loginTicket = "";

    /**
     * @var bool
     */
    protected bool $isLoggedIn = false;

    /**
     *
     * @throws AspectNotFoundException
     */
    public function initializeAction(): void
    {
        parent::initializeAction();

        $this->context = GeneralUtility::makeInstance(Context::class);
        $this->isLoggedIn = $this->context->getPropertyFromAspect('frontend.user', 'isLoggedIn', false);

        if ($this->isLoggedIn) {
            $this->loginTicket = $GLOBALS['TSFE']->fe_user->user['sso_ticket'];
        }

    }

    /**
     * @param appButtonListRepository $appButtonListRepository
     */
    public function injectAppButtonListRepository(AppButtonListRepository $appButtonListRepository): void
    {
        $this->appButtonListRepository = $appButtonListRepository;
    }

    /**
     * action list
     *
     * @return void
     */
    public function listAction(): ResponseInterface
    {

        $appButtonLists = $this->appButtonListRepository->findAll();

        if (count($appButtonLists) == 0) {
            throw new UnexpectedValueException('Keine Button Liste vorhanden!');
        }

        // we only use the frist list
        $appButtonList = $appButtonLists[0];
        $appButtons = $appButtonList->getAppButton();

        if (count($appButtons) == 0) {
            throw new UnexpectedValueException('Keine Buttons in der Liste vorhanden!');
        }

        $rendertButtonList = [];

        foreach ($appButtons as $key=>$appButton) {

            $rendertButtonList[$key]['name'] = $appButton->getButtonName();
            $rendertButtonList[$key]['description'] = $appButton->getButtonDescription();
            $rendertButtonList[$key]['sso'] = $appButton->getButtonSso();
            $rendertButtonList[$key]['icon'] = $appButton->getButtonImage();
            $rendertButtonList[$key]['uid'] = $appButton->getUid();

            // if we logged in as fe user, use ButtonLinkLoggedIn
            if($this->isLoggedIn) {
                // check if we use sso, and we have a login link with #ticket#
                if ($appButton->getButtonSso() AND
                    str_contains($appButton->getButtonLinkLoggedIn(), '#ticket#') AND
                    !empty($this->loginTicket)
                ) {
                    $rendertButtonList[$key]['url'] = ""; // Use openSsoAction
                }
                else {
                    $rendertButtonList[$key]['url'] = $appButton->getButtonLinkLoggedIn();
                }
            }

            // no url isset, so try ButtonLinkLoggedOut
            if (empty($rendertButtonList[$key]['url'])) {
                $rendertButtonList[$key]['url'] = $appButton->getButtonLinkLoggedOut();
            }

            // IDs are internal Link, so set url target
            if (!is_numeric($rendertButtonList[$key]['url'])) {
                $rendertButtonList[$key]['url_target'] = "_blank";
            }
            else {
                $rendertButtonList[$key]['url_target'] = "_self";
            }
        }

        $this->view->assign('appButtonList', $rendertButtonList);
        $this->view->assign('appButtons', $appButtons);
        return $this->htmlResponse();

    }

    /**
     * openSso
     *
     * Generate uri with login ticket from logged-in user and redirect to this uri
     *
     * @param AppButton $appButton
     * @return ResponseInterface
     */
    public function openSsoAction(AppButton $appButton): ResponseInterface {

        // if we logged in as fe user, use ButtonLinkLoggedIn
        if($this->isLoggedIn) {
            // check if we use sso in, and we have a login link with #ticket#
            if (str_contains($appButton->getButtonLinkLoggedIn(), '#ticket#') AND
                !empty($this->loginTicket)
            ) {
                $uri = str_replace( '#ticket#', $this->loginTicket, $appButton->getButtonLinkLoggedIn());
            }
            else {
                $uri = $appButton->getButtonLinkLoggedIn();
            }

            return $this->responseFactory->createResponse(303)
                ->withHeader('Location', $uri);

        }
        // else this is an error - redirect to list
        else {
            return (new \TYPO3\CMS\Extbase\Http\ForwardResponse('list'));
        }

    }
}
