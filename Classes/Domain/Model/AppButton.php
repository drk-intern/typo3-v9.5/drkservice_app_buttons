<?php
namespace DrkService\DrkserviceAppButtons\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Domain\Model\FileReference;
use TYPO3\CMS\Extbase\Annotation\ORM\Cascade;
/***
 *
 * This file is part of the "DRK Service App Buttons" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 André Gyöngyösi <a.gyoengyoesi@drkservice.de>, DRK Service GmbH
 *
 ***/
/**
 * App Button for DRK-Intern
 */
class AppButton extends AbstractEntity
{

    /**
     * buttonName
     *
     * @var string
     */
    protected string $buttonName = '';

    /**
     * buttonDescription
     *
     * @var string
     */
    protected string $buttonDescription = '';

    /**
     * buttonImage
     *
     * @var FileReference
     * @Cascade("remove")
     */
    protected ?FileReference $buttonImage = null;

    /**
     * buttonLinkLoggedIn
     *
     * @var string
     */
    protected string $buttonLinkLoggedIn = '';

    /**
     * buttonLinkLoggedOut
     *
     * @var string
     */
    protected string $buttonLinkLoggedOut = '';

    /**
     * buttonSso
     *
     * @var bool
     */
    protected bool $buttonSso = false;

    /**
     * Returns the buttonName
     *
     * @return string $buttonName
     */
    public function getButtonName(): string
    {
        return $this->buttonName;
    }

    /**
     * Sets the buttonName
     *
     * @param string $buttonName
     * @return void
     */
    public function setButtonName(string $buttonName): void
    {
        $this->buttonName = $buttonName;
    }

    /**
     * Returns the buttonDescription
     *
     * @return string $buttonDescription
     */
    public function getButtonDescription(): string
    {
        return $this->buttonDescription;
    }

    /**
     * Sets the buttonDescription
     *
     * @param string $buttonDescription
     * @return void
     */
    public function setButtonDescription(string $buttonDescription): void
    {
        $this->buttonDescription = $buttonDescription;
    }

    /**
     * Returns the buttonImage
     *
     * @return FileReference|null $buttonImage
     */
    public function getButtonImage(): ?FileReference
    {
        return $this->buttonImage;
    }

    /**
     * Sets the buttonImage
     *
     * @param FileReference $buttonImage
     * @return void
     */
    public function setButtonImage(FileReference $buttonImage): void
    {
        $this->buttonImage = $buttonImage;
    }

    /**
     * Returns the buttonLinkLoggedIn
     *
     * @return string $buttonLinkLoggedIn
     */
    public function getButtonLinkLoggedIn(): string
    {
        return $this->buttonLinkLoggedIn;
    }

    /**
     * Sets the buttonLinkLoggedIn
     *
     * @param string $buttonLinkLoggedIn
     * @return void
     */
    public function setButtonLinkLoggedIn(string $buttonLinkLoggedIn): void
    {
        $this->buttonLinkLoggedIn = $buttonLinkLoggedIn;
    }

    /**
     * Returns the buttonLinkLoggedOut
     *
     * @return string $buttonLinkLoggedOut
     */
    public function getButtonLinkLoggedOut(): string
    {
        return $this->buttonLinkLoggedOut;
    }

    /**
     * Sets the buttonLinkLoggedOut
     *
     * @param string $buttonLinkLoggedOut
     * @return void
     */
    public function setButtonLinkLoggedOut(string $buttonLinkLoggedOut): void
    {
        $this->buttonLinkLoggedOut = $buttonLinkLoggedOut;
    }

    /**
     * Returns the buttonSso
     *
     * @return bool buttonSso
     */
    public function getButtonSso(): bool
    {
        return $this->buttonSso;
    }

    /**
     * Sets the buttonSso
     *
     * @param bool $buttonSso
     * @return void
     */
    public function setButtonSso(bool $buttonSso): void
    {
        $this->buttonSso = $buttonSso;
    }

    /**
     * Returns the boolean state of buttonSso
     *
     * @return bool buttonSso
     */
    public function isButtonSso(): bool
    {
        return $this->buttonSso;
    }
}
