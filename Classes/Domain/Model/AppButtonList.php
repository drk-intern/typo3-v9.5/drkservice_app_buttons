<?php
namespace DrkService\DrkserviceAppButtons\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use TYPO3\CMS\Extbase\Annotation\ORM\Cascade;
/***
 *
 * This file is part of the "DRK Service App Buttons" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 André Gyöngyösi <a.gyoengyoesi@drkservice.de>, DRK Service GmbH
 *
 ***/
/**
 * App Button List for DRK-Intern
 */
class AppButtonList extends AbstractEntity
{

    /**
     * appButton
     *
     * @var ObjectStorage<AppButton>
     * @Cascade("remove")
     */
    protected ?ObjectStorage $appButton = null;

    /**
     * __construct
     */
    public function __construct()
    {

        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects(): void
    {
        $this->appButton = new ObjectStorage();
    }

    /**
     * Adds a AppButton
     *
     * @param AppButton $appButton
     * @return void
     */
    public function addAppButton(AppButton $appButton): void
    {
        $this->appButton->attach($appButton);
    }

    /**
     * Removes a AppButton
     *
     * @param AppButton $appButtonToRemove The AppButton to be removed
     * @return void
     */
    public function removeAppButton(AppButton $appButtonToRemove): void
    {
        $this->appButton->detach($appButtonToRemove);
    }

    /**
     * Returns the appButton
     *
     * @return ObjectStorage|null $appButton
     */
    public function getAppButton(): ?ObjectStorage
    {
        return $this->appButton;
    }

    /**
     * Sets the appButton
     *
     * @param ObjectStorage<AppButton> $appButton
     * @return void
     */
    public function setAppButton(ObjectStorage $appButton): void
    {
        $this->appButton = $appButton;
    }
}
